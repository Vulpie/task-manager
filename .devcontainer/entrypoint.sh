#!/bin/bash

if [ ! -f vendor/autoload.php ]; then
    composer install --no-progress --no-interaction
fi

if [ ! -f ".env" ]; then
    echo "Creating env file from $APP_ENV"
    cp .env.example .env
fi

php artisan migrate
php artisan key:generate
php artisan cache:clear
php artisan config:clear
php artisan route:clear

php artisan serve --port=8000 --host=0.0.0.0
exec docker-php-entrypoint "$@"
