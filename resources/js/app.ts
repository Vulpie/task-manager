import { createApp } from 'vue';
import 'vuetify/styles';
import '@mdi/font/css/materialdesignicons.css';
import { createVuetify } from 'vuetify';
import { createRouter, createWebHashHistory } from 'vue-router'
import { aliases, mdi } from 'vuetify/iconsets/mdi';
import * as components from 'vuetify/components';
import * as directives from 'vuetify/directives';

import App from './components/App.vue';
import Home from './components/pages/Home.vue';
import Week from './components/pages/Week.vue';
import Tasks from './components/pages/Tasks.vue';
import AppSettings from './components/pages/AppSettings.vue';

const vuetify = createVuetify({
    components,
    directives,
    theme: {
        defaultTheme: localStorage.getItem('use_dark_theme') === 'true' ? 'dark' : 'light'
    },
    icons: {
        defaultSet: 'mdi',
        aliases,
        sets: {
            mdi,
        },
    },
});

const routes = [
    { path: '/', component: Home },
    { path: '/week', component: Week },
    { path: '/tasks', component: Tasks },
    { path: '/app_settings', component: AppSettings },
];

const router = createRouter({
    history: createWebHashHistory(),
    routes,
})

createApp(App)
    .use(vuetify)
    .use(router)
    .mount('#app');