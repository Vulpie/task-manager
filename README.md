# 📗 Table of Contents

[[_TOC_]]

# 📖 TaskManager <a name="about-project"></a>

Task Manager is a user-friendly project designed to streamline task creation, management, and scheduling. With this tool, users can effortlessly create and organize tasks, define various task types to cater to different needs, and schedule tasks based on customizable patterns. Whether you're organizing your daily routines, managing projects, or planning events, Task Manager provides a versatile solution to enhance productivity and efficiency. Take control of your schedule and tasks with ease using Task Manager.

## 🛠 Built With <a name="built-with"></a>

### Tech Stack <a name="tech-stack"></a>

<details>
  <summary>Server</summary>
  <ul>
    <li><a href="https://laravel.com/">Laravel 10</a></li>
  </ul>
</details>

<details>
  <summary>Client</summary>
  <ul>
    <li><a href="https://vuejs.org/">Vue.js</a></li>
    <li><a href="https://vuetifyjs.com/en/">vuetify</a></li>
    <li><a href="https://www.typescriptlang.org/">TypeScript</a></li>
  </ul>
</details>

<details>
<summary>Database</summary>
  <ul>
    <li><a href="https://www.mysql.com/">MySQL</a></li>
  </ul>
</details>



